import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {RouteResolver} from './services/route.resolver';


const routes: Routes = [

  { path: '',
    component: HomeComponent
  },

  { path: 'dash',
    component: DashboardComponent,
    resolve: {
      routeResolver: RouteResolver
    },
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
