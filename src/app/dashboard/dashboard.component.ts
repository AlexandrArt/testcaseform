import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FormModel} from '../models/form.model';
import {ServerService} from '../services/server.service';
import {catchError, finalize, tap} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';
import {ResponseModel} from '../models/response.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  private dataForm: FormGroup;
  private errorList = {};
  tableData: ResponseModel;
  loading = false;

  constructor(private fb: FormBuilder,
              private serverService: ServerService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {

    /**
     * Создание формы
     */
    this.createForm();

    /**
     * Получение данных от Resolver
     */
    this.route.data.subscribe(
      (data) => {
        if (data.routeResolver) {
          this.fillForm(data.routeResolver.fields);
          if (data.routeResolver.error) {
            this.handleError(data.routeResolver.error);
          } else {
            this.tableData = new ResponseModel();
            this.tableData.data = data.routeResolver.data;
          }
        }
      }
    );
  }

  private createForm() {
    this.dataForm = this.fb.group({
      date: [''],
      first_name: [''],
      last_name: ['']
    });
  }

  /**
   * Отправка формы
   */
  sendData() {
    // Показывваем спинер
    this.loading = true;
    // Timeout только для показа спинера и имитации задержки сервера
    setTimeout(() => {
      // Получаем данные формы
      let formData: FormModel;
      formData = new FormModel();
      formData.date = this.dataForm.get('date').value;
      formData.first_name = this.dataForm.get('first_name').value;
      formData.last_name = this.dataForm.get('last_name').value;

      // Обнуляем список ошибок формы
      this.errorList = {};

      this.serverService.sendFormData(formData).pipe(
        tap(response => {
          this.tableData = new ResponseModel();
          this.tableData.data = response.data;
          this.serializeForm(this.dataForm);
        }),
        catchError(error => this.handleError(error)),
        finalize(() =>
          this.loading = false
        )
      ).subscribe();
    }, 3000);

  }

  /**
   * Обработка ошибок
   * @param error
   */
  handleError(error: HttpErrorResponse) {
    console.error(error);
    if (error.error instanceof ErrorEvent) {
      alert('Client problem: ' + error.error.message);
    } else {
      // Если 500 ошибка, выводим alert
      if (error.status === 500) {
        alert('Server problem');
      }
      this.errorList = error.error.error;
    }
    return throwError('Error throw');
  }

  /**
   * Проверка на наличие проверяемого поля в списке ошибок
   * @param filed - проверяемое поле
   */
  containInErrorList(filed) {
    return this.errorList[filed] !== undefined;
  }

  /**
   * Получение текста ошибки
   * @param field - поле, для которого получаем текст
   */
  getErrorMsg(field) {
    return this.errorList[field][0];
  }

  /**
   * Сериализация формы и установка параметров в URL
   * @param dataForm
   */
  private serializeForm(dataForm: FormGroup) {
    const formValues = dataForm.value;
    this.router.navigate([], {
      queryParams: formValues,
      queryParamsHandling: 'merge',
    });
  }

  /**
   * Заполнение формы данными на основе переданной Zмодели
   * @param fields
   */
  private fillForm(fields: FormModel) {
    this.dataForm.setValue(fields);
  }
}
