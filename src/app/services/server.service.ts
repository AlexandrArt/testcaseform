import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {FormModel} from '../models/form.model';
import {ResponseModel} from '../models/response.model';


@Injectable()
export class ServerService {

  constructor(private http: HttpClient) { }

  sendFormData(formData: FormModel): Observable<ResponseModel> {
    return this.http.post<ResponseModel>(environment.apiServer, formData);
  }

}
