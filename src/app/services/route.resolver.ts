import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {ServerService} from './server.service';
import {FormModel} from '../models/form.model';
import {catchError, map} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable()
export class RouteResolver implements Resolve<any> {

  constructor(public serverService: ServerService) { }

  resolve(route: ActivatedRouteSnapshot) {

    const recievedFormData: FormModel = route.queryParams as FormModel;

    // Если есть какие-то параметры в адресной строке
    // Возвращаем data или error + fields для заполнения формы
    if (Object.keys(recievedFormData).length !== 0) {
      return this.serverService.sendFormData(recievedFormData).pipe(
        map(product =>  {
          return { data: product.data, fields: recievedFormData};
        }),
        catchError(e => {
          return of({ error: e, fields: recievedFormData });
        })
      );
    }
  }

}
